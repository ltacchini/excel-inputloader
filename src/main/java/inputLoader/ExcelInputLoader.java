package inputLoader;

import org.apache.poi.ss.usermodel.*;

import lectura.InputLoader;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ExcelInputLoader implements InputLoader { 
	
    static Sheet sheet;
    int rowNumber;
    
    public ExcelInputLoader() { } // FIXME: esto hay que sacarlo. 
    
    public ExcelInputLoader (URL url) {
    	Workbook workbook;
		try {
			workbook = WorkbookFactory.create(new File(url.getPath()));
			String sheetname = url.getRef();
		    ExcelInputLoader.sheet = workbook.getSheet(sheetname);
		    workbook.close();
		} 
		catch (Exception e) {
			e.printStackTrace();
		} 
    }
    
	@Override
	public Object[] next() {
		Row row = sheet.getRow(rowNumber);
		List<Object> result = new ArrayList<>();
		if (row != null) {
			rowNumber++;
			row.forEach(cell -> {
				result.add(getCellValue(cell));
			});
		}
		return result.toArray();
	}
	
	@Override
	public boolean hasNext() {
		return rowNumber < sheet.getPhysicalNumberOfRows();
	}
	
	private static Object getCellValue(Cell c) {
		CellType type = c.getCellType();
		
		switch(type) {
			case STRING: return c.getStringCellValue();
				
			case NUMERIC:
				if (!DateUtil.isCellDateFormatted(c)) 
					return (int) c.getNumericCellValue();
		        
				SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm"); // FIXME: revisar el DateFormat para que sea lo más genérico posible. 
	            return dateFormat.format(c.getDateCellValue());
				
			case BOOLEAN: return c.getBooleanCellValue();
			
			case BLANK: return "";
				
			default: throw new RuntimeException("Formato no valido: por favor, revise la planilla de excel: " + c.getNumericCellValue());
		}
	}

}

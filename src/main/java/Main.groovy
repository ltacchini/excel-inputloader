import config.DefaultConfig
import inputLoader.ExcelInputLoaderFactory
import lectura.InputLoader

DefaultConfig fileConfig = new DefaultConfig("config.properties");
String path = fileConfig.getElement("path");
String sheetName = fileConfig.getElement("sheet");

URL url = new URL("file:"+path+"#"+sheetName);
System.out.println(url);

ExcelInputLoaderFactory inputLoaderFactory = new ExcelInputLoaderFactory()
InputLoader il = inputLoaderFactory.createInputLoader(url)

for (Object[] line : il) {
	println line
	println il.hasNext()
}



package config;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class DefaultConfig implements Config {

	private Properties props;

	public DefaultConfig(String configPath) throws IOException {
		this.props = new Properties();
		this.props.load(new FileReader(configPath));
	}

	@Override
	public String getElement(String key) {
		return props.getProperty(key);
	}

}